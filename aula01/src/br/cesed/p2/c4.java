package br.cesed.p2;

import br.cesed.p1.*;

class c4 {

	public static void main(String[] args) {
		c1 classe1 = new c1();
		classe1.pubM();
		classe1.protM();
		classe1.defM();
		classe1.privM();
		
		c2 classe2 = new c2();
		
		c3 classe3 = new c3();
		classe3.pubM();
		classe3.protM();
		classe3.defM();
		classe3.privM();
	}
	
	/** vis�vel para todos */
	public void pubM() {}

	/** vis�vel para heran�a e pacote */
	protected void protM() {}

	/** vis�vel para classes no mesmo pacote */
	void defM() {}

	/** vis�vel apenas nesta classe */
	private void privM() {}
}

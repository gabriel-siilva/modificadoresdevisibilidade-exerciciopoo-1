package br.cesed.p1;

import br.cesed.p2.*;

class c2 {

	public static void main(String[] args) {
		c1 classe1 = new c1();
		classe1.pubM();
		classe1.protM();
		classe1.defM();
		classe1.priv();
		
		c3 classe3 = new c3();
		classe3.pubM();
		classe3.protM();
		classe3.defM();
		classe3.privM();
		
		c4 classe4 = new c4();
	}
	
	/** vis�vel para todos */
	public void pubM() {}

	/** vis�vel para heran�a e pacote */
	protected void protM() {}

	/** vis�vel para classes no mesmo pacote */
	void defM() {}

	/** vis�vel apenas nesta classe */
	private void privM() {}
}

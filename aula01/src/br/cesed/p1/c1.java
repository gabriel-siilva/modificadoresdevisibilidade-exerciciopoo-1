package br.cesed.p1;

import br.cesed.p2.*;

public class c1 {
	
	public static void main(String[] args) {
		
		/** 
		 * a classe c2 est� com a visibilidade default
		 * e no mesmo pacote que a classe c1 por isso n�o houve
		 * problemas e nem precisou ser importada
		 */
		c2 classe2 = new c2();
		classe2.pubM();
		classe2.protM();
		classe2.defM();		
		
		/**
		 * met�do privado s� pode ser usado 
		 * apenas na pr�pria classe 
		 */
		classe2.privM();
		
		/**
		 * a classe c3 est� com visibilidade public e foi importada
		 * por isso n�o teve nenhum erro ao ser instanciada
		 */
		c3 classe3 = new c3();
		classe3.pubM();
		
		/**
		 * o m�todo protected s� pode ser chamado
		 * por classes no mesmo pacote ou heran�a
		 */
		classe3.protM();
		
		/**
		 * o m�todo default s� pode ser chamado
		 * por classes no mesmo pacote
		 */
		classe3.defM();
		
		/**
		 * o m�todo private s� pode ser usado na pr�pria classe
		 */
		classe3.privM();
		
		/**
		 * a classe c4 est� com a visibilidade default
		 * s� pode ser instanciada por uma classe no mesmo pacote
		 */
		c4 classe4 = new c4();
		
	}
	
	/** vis�vel para todos */
	public void pubM() {}

	/** vis�vel para heran�a e pacote */
	protected void protM() {}

	/** vis�vel para classes no mesmo pacote */
	void defM() {}

	/** vis�vel apenas nesta classe */
	private void privM() {}

}
